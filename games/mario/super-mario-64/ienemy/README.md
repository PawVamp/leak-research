# Super Mario 64 (N64) - iEnemy

Files are in `bbgames.7z/sm64/ienemy`.

An empty folder. The compiled file in the parent directory does contain information and the folder is referenced in other places, but this Leak does not contain any files at this location, probably because those were not needed for the translation of the game.