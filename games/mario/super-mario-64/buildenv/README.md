# Super Mario 64 (N64) - BuildEnv

Files are in `bbgames.7z/sm64/buildenv`.

Probably contains built ROM files, however the file throws error when trying to play on Mupen64.

## Readable files
 Filename | Content 
----------|---------

## Other important files
The `libultra_rom.a` probably contains an unknown build of SM64, maybe Shindou, it may also be a Beta- or other version. The purpose of `makemask` is completely unknown.

## Complete file list
```
libultra_rom.a
makemask
```