# NEWS tapes
The leaks contain a series of tape backup archives at the path:

    other.7z/NEWS/テープリストア/

The archives are numbered, and only some are present. The file `list.txt` contains import logs, including the original archive label, and noting that most of the 41 tapes failed to import (with the ones below being the only successful ones).

## Tape listing

| Name | Label | Contents |
| --- | --- | --- | 
| NEWS_02 | 1993/3/22 DONKEY BACKUP | Various e-mail attachments, see [news-02.md](./news-02.md) |
| NEWS_04 | 1995/10/5  有本NEWS バックアップ（CAD2）| Developer home directories | 
| NEWS_05 | (none) | Developer home directories | 
| NEWS_09 | (none) | Developer home directories |
| NEWS_11 | 日野バックアップ | Home directory of [Shigefumi Hino](https://nintendo.fandom.com/wiki/Shigefumi_Hino) (seemingly) |
| NEWS_17 | 1991/12/24 ゼルダの伝説神々のトライフォース | Empty, presumably contained the LttP code now in `other.7z/SFC/ソースデータ/ゼルダの伝説神々のトライフォース/` |
| NEWS_41 | 1993/6/14 スーパーマリオコレクション | Empty, presumably contained the SMAS code now in `other.7z/SFC/ソースデータ/srd13-SFCマリオコレクション/` |