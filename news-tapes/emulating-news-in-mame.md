# Emulating NeWS Workstations
There are two operating systems that officially support Sony NeWS workstations:
Sony's NeWS OS, and NetBSD. NeWS workstations come in two flavors: Motorola
68k processors (like Macintosh, NeXT, SEGA Genesis), and MIPS processors
(like a lot of 90s SGI systems, Sony PS1, and Nintendo 64).

Identifying the types of binaries can be done with the Unix or Linux `file`
command.

68k
```sh
> file sbin/ifconfig
sbin/ifconfig: a.out big-endian 32-bit demand paged executable
```

MIPS
```sh
> file bin/sh
bin/sh: MIPSEB ECOFF executable (paged) stripped - version 2.11
```

First, install MAME per the [instructions](https://docs.mamedev.org/). This
guide is written with Linux in mind, but the CLI is the same for the Windows and
Macintosh versions. You can use MAME's GUI or CLI to launch, but this guide will
use the CLI.

Next, obtain the ROMs for the system you want to emulate and put them into the
`roms` directory within `mame`. MAME can use archives as long as they are named
appropriately. Place the desired disk image wherever desired. The example
commands assume they are in the same folder as the `mame64` executable, but this
is not a requirement. NeWS-OS is not public domain or open-source, but you can
get NetBSD from the following locations:
- [NetBSD 68k](http://wiki.netbsd.org/ports/news68k/)
- [NetBSD MIPS](http://wiki.netbsd.org/ports/newsmips/)
NetBSD can't run NeWS-OS binaries, but can manipulate the filesystems to a
limited extent. Multi-floppy NetBSD installers require the second floppy image
to be padded to 1.44MB for MAME to recognize it as a valid disk image.


MAME currently emulates two NeWS workstations:
- NWS-1580 (Motorola 68030), machine name in MAME is nws1580
- NWS-3260 (MIPS R3000), machine name in MAME is nws3260

To start, you can launch an NWS workstation without any OS and boot to the
monitor ROM. You won't be able to do much, but if it loads, that means the ROMs
are set correctly and MAME is working.
```bash
> ./mame64 nws1580
> ./mame64 nws3260
```

Once the system has booted to NeWS Monitor, you can type `help` to see the
available commands. Without an OS image, you won't be able to do much.

Next, configure the slots and devices on the system. If you have a NeWS-OS hard
disk image, you can attach it by running this command:
```bash
> ./mame64 <news_machine> -hard <img_file_name>
```

If you have a floppy image, use the `-flop` option instead. You can view the
possible peripherial connections by running:
```bash
> ./mame64 <news_machine> -listslots
```

Then, you can use the slot names as command line switches to select the attached
devices. Below is an example of connecting two hard drives and a CD-ROM drive.
SCSI 0 defaults to harddisk, so it doesn't have to be specified.
```bash
> ./mame64 nws3260 -scsi:1 harddisk -scsi:6 cdrom -listmedia

SYSTEM           MEDIA NAME       (brief)    IMAGE FILE EXTENSIONS SUPPORTED
---------------- --------------------------- -------------------------------
nws3260          floppydisk       (flop)     .dsk  .ima  .img  .ufi  .360  .d77  .d88  .1dd  .dfi  .hfe  .imd  .ipf  .mfi  .mfm  .td0  .cqm  .cqi
                 harddisk1        (hard1)    .chd  .hd   .hdv  .2mg  .hdi
                 harddisk2        (hard2)    .chd  .hd   .hdv  .2mg  .hdi
                 cdrom            (cdrm)     .chd  .cue  .toc  .nrg  .gdi  .iso  .cdr
> ./mame64 nws3260 -scsi:1 harddisk -scsi:2 cdrom -hard1 <disk_img> -flop <floppy_img>
```
Note that since there are two hard drives now, `-hard` no longer works, you must
specify them by index (`hard1` or `hard2`). Other than that, you can specify
images the same way as in the default configuration.

After booting with a hard drive or floppy image and reaching NeWS Monitor, there
are more things you can do.

## NeWS Monitor
- The main command you'll run from NeWS monitor is the `bo` command.
```
# Boot from hard disk
NEWS> bo
# Boot from hard disk in a different SCSI slot, for example SCSI ID 1 (default 0)
NEWS> bo sd(1)
# Boot from a floppy disk
NEWS> bo fh
```

- The MIPS NeWS monitor also supports disk inspection. Running the `di` command
(which can also be pointed to a specific device) will scan attached devices and
list the valid disklabel BSD partitions it finds. For example:
```
NEWS> di sd(0)
Tahoe disk label
a:        0     15884
b:    15884     73492
c:        0    349770
d:    89376     15881
e:   105260     15884
f:   121144    228626
g:    89376    260394
h:        0         0
```

## NeWS-OS
If all goes well, after running `bo` from the NeWS Monitor, you'll reach a login
prompt. On the disk image I had, I edited `/etc/passwd` to remove the root login
password, so I could just log in as root. You could also crack the root password,
replace it with a known password hash, etc. Once logged in, you have an early
BSD environment to play with. In general, old BSD documentation also applies to
NeWS-OS (with the addition of Japanese support and some Sony libraries). For
example, the `disklabel` utility allows for inspecting and modifying partitions.
You can use NetBSD (and probably other BSDs as well, although I only tried
NetBSD) on a modern computer or virtual machine to copy files to the disk image.
Modern Linux systems can usually mount ufs (with the appropriate kernel module),
the file system type used by BSD and NeWS-OS, but often exclusively read-only.

### NeWS-OS on MIPS
There is a CD-ROM ISO of NeWS-OS 4.2.1R floating around on the internet,
and you can inspect the ISO by mounting it. Most of the files are .Z archives.

The available ISO appears to be the AP-Bus version. While AP-Bus systems are
also MIPS-based, the NeWS-OS version seems to be different. The installer includes
model-based selection of the bootloader and kernel. The 3260 requires the MIPS
bootloader, whereas the 5000 series and 3100 use the AP-Bus bootloader. Even though
the installer media has the 68k and MIPS versions of the bootloader present in
the miniroot filesystem (traditionally used when installing Unix from tape), the
CD-ROM itself has the AP-Bus bootloader built in, making it incompatible with
68k and regular MIPS workstations.

The architecture letter does match (R = RISC, MIPS, C = CISC, 68k,
[source](https://katsu.watanabe.name/doc/sonynews/newsos.html)), so it is still
unclear if the files on the CD-ROM will work for the 3260 or not.

NeWS-OS 5.0 has been proven to work with MAME, but it uses an SVR4 kernel instead
of 4.2BSD, so it can't run older MIPS NeWS-OS binaries without BSD translation
libraries (still not sure how to enable this).

### NeWS-OS Boot Process
The NeWS-OS bootloader starts at offset 0x200 on valid disks. The first instruction,
at offset 0x0, has a pointer to 0x200 (disassembles to a branch instruction). This
is to skip over the BSD disklabel, which holds the partition information. The
bootloader will then load the kernel, called vmunix (4.2BSD) or unix (SVR4).

### Formatting hard drives on NeWS-OS 4.1
You can see how the bootloader and disklabel are constructed within the install
script contained in the miniroot filesystem (miniroot/etc/rc), starting on line
878. To write the bootloader to a disk and format it, do the following:
```sh
> cat boot00 boot0 | dd of=/dev/rsd<disk_num>a
> disklabel -w sd<disk_num> <hd_type_from_etc_disktab>
```
Then, you can use the `newfs` command on the partitions to complete formatting.
Check the `/etc/disktab` configuraton for specific partition layouts, or read
back the disklabel by running `disklabel -r /dev/sd<drive_num>a`.

Here is a full example, creating a bootable 632MByte 68k NeWS-OS disk attached to
SCSI ID 1
```sh
# On the host system, create an empty HD image.
# The number of sectors is C * H *S, which you can get from /etc/disktab
# In this case, it is 1356 * 14 * 69 = 1,309,896
> dd if=/dev/zero of=hd632.img bs=512 count=1309896
# Next, write the desired bootloader to the hard drive (you could do this from
# NeWS-OS as well, but I got an emulator hang every time I tried. Since this is
# not writing to a real disk, you have to tell dd not to truncate the file)
> cat /path/to/bootloader/boot00 /path/to/bootloader/boot0 | dd of=hd632.img conv=notrunc
# Next, start the emulator with this as one of the hard drives.
# From here, the commands will be run in NeWS-OS. NetBSD's latest `disklabel`
# overwrites part of the bootloader.
# Write the disklabel
> disklabel -w sd01 hd632
# Make the new filesystems
> newfs /dev/sd01a
# Optionally, to mirror the setup of the NeWS-OS installer, you can re-run the
# disklabel command here to blow away the partition table info for partition A.
# This makes it "hidden" in a way, because it will be labeled as unused. NeWS-OS
# doesn't seem to care one way or the other.
> newfs /dev/sd01d
> newfs /dev/sd01e
> newfs /dev/sd01f
# From here, you can mount the new disk in NeWS-OS, or use a modern NetBSD system
# to copy over the rest of the files needed to boot.
```

NetBSD's disklabels appear to be bigger than NeWS-OSes, so it will overwrite
the bootloader region if you write disklabels with NetBSD.

### Setting up networking
Since NeWS-OS is based on Unix, it has good networking support for an early-90s
OS. There are a few setup steps that must be taken to set up MAME for ethernet
use. First, configure the MAME binary to have the appropriate permissions to
communicate with the TUN/TAP interface. This example is for OpenSUSE, but should
be similar on other distros.

```bash
> sudo zypper install libcap-progs
> sudo setcap cap_net_raw,cap_net_admin=eip ./mame64
```

Next, download `taputil.sh` from the MAME source and configure the driver.
```bash
> wget https://raw.githubusercontent.com/bji/libmame/master/src/osd/sdl/taputil.sh
> chmod +x taputil.sh  # Make script executable
> export TAP=tap-mess-${UID}-0  # Set TAP/TUN user
> export IP_ADDR=192.168.3.30  # Set emulated platform IP
> export IP_ADDR_HOST=192.168.3.99  # Set computer IP on the TAP/TUN network
> sudo -s ./taputil.sh -d $USER  # Delete any pre-existing configs
> sudo -s ./taputil.sh -c $USER $IP_ADDR $IP_ADDR_HOST -  # Configure TAP/TUN
```

Then, start MAME and ensure that the `Network Devices->:net` option in the UI
menu (`Tab`) is set to `TAP/TUN Device`. Now, you can configure networking on
the emulated platform.

```sh
ifconfig en0 up  # Enable network interface
ifconfig en0 192.168.3.30  # Assign IP address
route -f  # Flush old route configuration, if any
route add default 192.168.3.99 0  # Assign host computer as the default route
```

Now, you should be able to ping your host system from the emulated platform, and
vice versa. You can also use utilities like `telnet` and `ftp`. Note - the 1580
was too slow to use, but the 3260 was fine. This might be a setup-specific
issue though.

For more about networking MAME in Linux, see the [MAMEDEV wiki](https://wiki.mamedev.org/index.php/Driver:Apollo#Networking)

On Windows, you can use [OpenVPN TAP/TUN driver](https://community.openvpn.net/openvpn/wiki/ManagingWindowsTAPDrivers) instead.

## Debugging an emulated NeWS workstation
MAME includes an integrated debugger. Simply add the `-debug` flag to the command
line when launching.

Helpful tools for debugging:
- [MAME debugger documentation](https://docs.mamedev.org/debugger/index.html)
- [Disasmips](http://acade.au7.de/disasmips/disasmips.htm), a free MIPS disassembler

## MAME tips
- Run `./mame64 -showusage` to see the command line options.
- When emulating computers, the MAME interface will not respond to keystrokes
like Tab because it has to pass them through to the emulated platform. To enter
a MAME command (like pressing Tab to bring up the menu), first press the Scroll
Lock key. Then, you can run MAME commands. Press Scroll Lock again to resume
sending all input to the emulated platform.

## Assorted notes
The CD-ROM drive appears to have been associated with SCSI ID 6, but the emulator
developer disabled it by default due to issues with NeWS-OS.

NeWS info:
- http://www.bitsavers.org/pdf/sony/news/NEWS_Brochures_1988.pdf
- https://katsu.watanabe.name/doc/sonynews/model.html
