# List of tools + credits

## Tools/files in this repository

| File | Author | Description |
| --- | --- | --- |
| [SOUtoOBJ21.py](./SOUtoOBJ21.py) / [SOUtoPLY31.py](./SOUtoPLY31.py) | smb123w64gb | convert Mario 64 model formats |
| [as65c_notes.txt](./as65c_notes.txt) | ndiddy | notes on AS65C syntax |
| [SF.xml](./SF.xml) | LuigiBlood | running builds of Starfox 2 without having to change the header every time on bsnes-plus |
| [AgbConvENGv3.ips](./AgbConvENGv3.ips) | prism | English patch for AgbConv |
| [texarray2bmp/](./texarray2bmp/) | cuckydev | Convert an array of pixel data to a bitmap file |
| [hex2bin_fixed.py](./hex2bin_fixed.py) | @xSke | Decoder for Nintendo's `.hex` format variant |
| [cgx2png/](./cgx2png/) | @xSke | Converter/reader for CGX, COL, and SCR graphics files |

## Other tools
| Name | Link | Description |
| --- | --- | --- |
| SCad | `bbgames.7z/marioAGB/graphic_data/srdcad/SCad.exe` | Internal Nintendo tool for loading and creating graphics files in various formats (including .cgx, .col, .scr, .obj files) |
| YY-CHR | https://w.atwiki.jp/yychr | Graphics viewer and editor for various NES/SNES-era formats |
| Tile Molester | https://www.romhacking.net/utilities/109/ | Graphics viewer and editor for various NES/SNES-era formats (requires Java) |
| OBJ-Viewer | https://github.com/TheLX5/OBJ-Viewer | Viewer for OBJ/OBX, CGX, COL files |
| SuperTileMapper | https://github.com/Dotsarecool/SuperTileMapper | SNES PPU memory viewer and editor, also supports loading the aforementioned formats |