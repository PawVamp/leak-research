#!/usr/bin/env python3

import argparse, struct, sys
from PIL import Image

def strip_scad_footer(bs):
    footer_index = bs.find(b"NAK1989 S-CG-CAD")
    if footer_index:
        return bs[0:footer_index]
    return bs

def decode_tile(data, i, depth):
    pixels = bytearray(8*8)

    for y in range(8):
        for x in range(8):
            # Pixel format is fuckin' weird.
            # https://web.archive.org/web/20071111200111/http://www.geocities.com/Qwertie256/attic/snesemu/qsnesdoc.html#GraphicsFormat
            for plane in range(depth):
                row = data[i + (plane >> 1) * 16 + y*2 + (plane & 1)]
                if row & (1 << (7-x)):
                    pixels[y*8 + x] |= 1 << plane

    return pixels

def decode_tileset(data, tile_depth):
    tiles = []
    tile_size = tile_depth * 8
    tile_count = len(data) // tile_size
    
    i = 0
    for tile in range(tile_count):
        tiles.append(decode_tile(data, i, tile_depth))
        i += tile_size

    return tiles

def decode_palette(data):
    colors = []
    for i in range(256):
        color = struct.unpack("<H", data[i*2:i*2+2])[0]

        # xBBBBBGG GGGRRRRR
        red = color & 0x1F
        green = (color >> 5) & 0x1F
        blue = (color >> 10) & 0x1F

        colors.append((red * 8, green * 8, blue * 8))
    return colors

def decode_tilemap(data):
    tiles = []

    for i in range(len(data) // 2):
        value = struct.unpack("<H", data[i*2:(i+1)*2])[0]

        # https://wiki.superfamicom.org/backgrounds
        # vhopppcc cccccccc
        # v/h        = Vertical/Horizontal flip this tile.
        # o          = Tile priority.
        # ppp        = Tile palette. The number of entries in the palette depends on the Mode and the BG.
        # cccccccccc = Tile number.

        vflip = (value & 0x8000) != 0
        hflip = (value & 0x4000) != 0
        priority = (value & 0x2000) != 0
        palette = (value >> 10) & 0x7
        tile = value & 0x3FF

        tiles.append((tile, palette, hflip, vflip))
    return tiles

def create_palette_image(palette):
    img = Image.new("RGB", (16, 16))
    for i in range(256):
        img.putpixel((i%16, i//16), palette[i])
    return img

def draw_tile(image, x, y, tiles, tile_index, palette, palette_offset, hflip, vflip):
    tile = tiles[tile_index]

    for py in range(8):
        for px in range(8):
            tile_x = px if not hflip else 7-px
            tile_y = py if not vflip else 7-py
            out_x = x * 8 + px
            out_y = y * 8 + py

            color = tile[tile_y*8+tile_x]
            if color > 0 or True:
                r, g, b = palette[(palette_offset + color) % len(palette)]
                image.putpixel((out_x, out_y), (r, g, b, 255))
            else:
                image.putpixel((out_x, out_y), (0, 0, 0, 0))

def draw_tile_16(image, x, y, tiles, tile_index, palette, palette_offset, hflip, vflip):
    draw_tile(image, x*2, y*2, tiles, tile_index, palette, palette_offset, hflip, vflip)
    draw_tile(image, x*2+1, y*2, tiles, tile_index + 1, palette, palette_offset, hflip, vflip)
    draw_tile(image, x*2, y*2+1, tiles, tile_index + 16, palette, palette_offset, hflip, vflip)
    draw_tile(image, x*2+1, y*2+1, tiles, tile_index + 16 + 1, palette, palette_offset, hflip, vflip)

def draw_tilemap(img, tilemap, tiles, palette, depth, size=(32, 32), offset=(0, 0), is_16=False, tilemap_offset=0):
    w, h = size
    ox, oy = offset
    for y in range(h):
        for x in range(w):
            tile_pos = tilemap_offset + y * w + x
            if tile_pos >= len(tilemap):
                print("warning: tile position exceeds tilemap boundaries ({:04x} > {:04x})".format(tile_pos, len(tilemap)))
                continue

            tile_index, palette_offset, hflip, vflip = tilemap[tile_pos]
            if tile_index >= len(tiles):
                print("warning: tile at ({},{}) indexes tile out of tileset ({:04x} > {:04x})".format(x, y, tile_index, len(tiles)))
                continue

            if depth == 8:
                # 256-color always uses full palette
                palette_offset = 0
            
            if is_16:
                draw_tile_16(img, x + ox, y + oy, tiles, tile_index, palette, palette_offset * 16, hflip, vflip)
            else:
                draw_tile(img, x + ox, y + oy, tiles, tile_index, palette, palette_offset * 16, hflip, vflip)

def create_bg_image(tilemap, tiles, palette, depth, is_16=False, tilemap_offset=0):
    w, h = (32, 32)
    if is_16:
        img = Image.new("RGBA", (w * 16, h * 16))
    else:
        img = Image.new("RGBA", (w * 8, h * 8))
    draw_tilemap(img, tilemap, tiles, palette, depth, (32, 32), (0, 0), is_16, tilemap_offset)
    return img

def create_bg_image_big(tilemap, tiles, palette, depth, is_16=False, tilemap_offset=0):
    w, h = (64, 64)

    if is_16:
        img = Image.new("RGBA", (w * 16, h * 16))
    else:
        img = Image.new("RGBA", (w * 8, h * 8))

    draw_tilemap(img, tilemap, tiles, palette, depth, (32, 32), (0, 0), is_16, tilemap_offset)
    draw_tilemap(img, tilemap, tiles, palette, depth, (32, 32), (32, 0), is_16, tilemap_offset + 1024)
    draw_tilemap(img, tilemap, tiles, palette, depth, (32, 32), (0, 32), is_16, tilemap_offset + 2048)
    draw_tilemap(img, tilemap, tiles, palette, depth, (32, 32), (32, 32), is_16, tilemap_offset + 3072)
    return img

def create_tileset_image(tiles, palette, width, palette_offset=0):
    img_width_tiles = width
    img_height_tiles = len(tiles) // img_width_tiles

    img = Image.new("RGBA", (img_width_tiles * 8, img_height_tiles * 8))

    tile_idx = 0
    for y in range(img_height_tiles):
        for x in range(img_width_tiles):
            if tile_idx < len(tiles):
                draw_tile(img, x, y, tiles, tile_idx, palette, palette_offset, False, False)
            tile_idx += 1

    return img

def main():
    parser = argparse.ArgumentParser(description="Decode Nintendo graphics files.")
    parser.add_argument("-t", "--tiles", help="*.cgx tile graphics file", type=argparse.FileType("rb"), required=True, dest="tiles")
    parser.add_argument("-p", "--pal", "--palette", help="*.col color palette file", type=argparse.FileType("rb"), dest="palette")
    parser.add_argument("-m", "--map", "--tilemap", help="*.scr tilemap file", type=argparse.FileType("rb"), dest="tilemap")
    parser.add_argument("-d", "--depth", help="tile bit depth (2/4/8)", type=int, choices=[2, 4, 8], dest="depth", required=True)
    parser.add_argument("--double", help="16x16 (double) tiles?", type=bool, dest="double")
    parser.add_argument("-o", "--out", help="output png file", type=argparse.FileType("wb"), required=True, dest="out")
    parser.add_argument("-w", "--width", help="image width (in tiles) when drawing plain tilesets", type=int, default=16)
    args = parser.parse_args()

    tiles = decode_tileset(strip_scad_footer(args.tiles.read()), args.depth)
    print("tiles from {}: {} tiles, {}-bit color".format(args.tiles.name, len(tiles), args.depth))

    if args.palette:
        palette = decode_palette(strip_scad_footer(args.palette.read()))
        print("palette from {}: {} non-blank colors".format(args.palette.name, len([p for p in palette if p != (0, 0, 0)])))
    else:
        palette = None

    if args.tilemap:
        if not palette:
            print("error: must provide color palette (--palette) when rendering tilemap")
            sys.exit(1)

        tilemap = decode_tilemap(strip_scad_footer(args.tilemap.read()))
        print("tilemap from {}".format(args.tilemap.name))

        print("rendering tilemap...")
        img = create_bg_image(tilemap, tiles, palette, args.depth, (0, 0), (32, 32), is_16=args.double)
    else:
        print("rendering tiles in rows of {}...".format(args.width))
        img = create_tileset_image(tiles, palette, args.width, False)

    img.save(args.out)
    print("done! wrote image file to {}. :)".format(args.out.name))

if __name__ == "__main__":
    main()