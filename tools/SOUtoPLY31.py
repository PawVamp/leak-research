import sys
import os
import struct


f = open(sys.argv[1], 'r')
SR = 0
GR = 0
LR = 0

VOBJ = [[],[]]
TOBJ = [[],[],[]]

LGT = []
VTX = []
TRI = []
triOff = 0
curLGT = []
curV = 0
curT = 0
uvScale = [1,1]
for x in f:
        if(GR):
                if(x.find("gsSPLight(")>=0):
                        if(len(LGT)):
                                curLGT = LGT[int(x[x.find("[")+1:x.find("]")])]
                elif(x.find("gsSPVertex(")>=0):
                        triOff = int(x[x.find("[")+1:x.find("]")])
                        if(curT==0):
                                name = x[x.find('&')+1:x.find('[')]
                                print(name)
                                for i,vtxN in enumerate(VOBJ[0]):
                                        print(vtxN)
                                        if(vtxN == name):
                                                
                                                curV = i
                                                TOBJ[2].append(curV)
                                                curT = 1
                elif(x.find("gsSP1Triangle(")>=0):
                        tr = x.replace("\tgsSP1Triangle(","").replace("),\n","").split(',')
                        tr.pop()
                        tf = []
                        for t in tr:
                                t = int(t) + triOff
                                if(len(curLGT)>0):
                                        for a in range(3):
                                                #print curV
                                                VOBJ[1][curV][t][6+a] = curLGT[a]
                                        for a in range(2):
                                                VOBJ[1][curV][t][4+a] = float(int(VOBJ[1][curV][t][4+a])>>6)
                                                VOBJ[1][curV][t][4+a] /= uvScale[a]
                                tf.append(t)
                        TRI.append(tf)
                elif(x.find("LoadTextureImage")>=0):
                        uvScale = list(map(float,x[x.find("(")+1:x.find(")")].split(',')[3:5]))
                        uvScale[0] -= 1
                        uvScale[1] -= 1
                #else: print(x)
        if(SR):
                #print x
                if(x.find('};') == -1):
                        VTX.append(list(map(float,x[x.find('{')+1:x.find('}')].split(','))))
                '''if(len(x[x.find('{')+1:x.find('}')].split(','))>3):
                        if((x[x.find('{')+1:x.find('}')].split(',')[4])>1):
                                print(x[x.find('{')+1:x.find('}')].split(',')[4])'''
        if(LR):
                #print x[x.find('(')+1:x.find(')')].split(',')
                LGT.append(x[x.find('(')+1:x.find(')')].split(','))
                
        if(x.find('Gfx ') >= 0 ):
                GR = 1
                TOBJ[0].append(x[x.find("Gfx ")+4:x.find("[")])
                print(x[x.find("Gfx ")+4:x.find("[")])
        if(x.find('Lights1') >= 0 ):
                #print x
                LR = 1
        if(x.find('static Vtx') >= 0):
                SR = 1
                VOBJ[0].append(x[x.find("Vtx ")+4:x.find("[")])
        if(x.find('};') == 0):
                if(SR): 
                        VOBJ[1].append(VTX)
                        VTX = []
                if(GR):
                        TOBJ[1].append(TRI)
                        TRI = []
                SR = 0
                curT = 0
                GR = 0
                LR = 0
                #print "end"
        
#VTX.pop()
for e in range(len(TOBJ[2])):
        #print(TOBJ[2][e])
        VTX = VOBJ[1][TOBJ[2][e]]
        TRI = TOBJ[1][e]
        #print e
        o = open(TOBJ[0][e] + ".ply", 'w')
        o.write("ply\nformat ascii 1.0\n")
        o.write("element vertex %i\n"% len(VTX) )
        for a in ["x","y","z","s","t"]:
                o.write("property float %s\n" % a)
        for a in ["red","green","blue","alpha"]:
                o.write("property uchar %s\n" % a)
        o.write("element face %i\n"% len(TRI) )
        o.write("property list uchar uint vertex_indices\nend_header\n")
        curv=0
        #print("element vertex %i\n"% len(VTX) )
        for v in VTX:
                curv+=1
                #print(curv)
                i = []
                for vl in v:
                        i.append(vl)
                o.write("%s %s %s %s %s %i %i %i %i\n" % (i[0],i[1],i[2],i[4],i[5],int(i[6]),int(i[7]),int(i[8]),int(i[9])))


        for p in TRI:
                o.write("3 %i %i %i\n" % (p[0],p[1],p[2]))
